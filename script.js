// ==UserScript==
// @name ProtonMail Composer Formatter
// @namespace bhr
// @version 0.1
// @description formats text
// @author ben romney
// @match https://mail.protonmail.com/*
// @grant none
// ==/UserScript==

(function() { console.clear(); var styles = 'color:steelblue !important; font-family:menlo, consolas, courier new, monospace, sans-serif !important;'; // append / change additional styles to the string above; the code below should do the rest

var mutation, observer, i, doneKey = 'bhr-is-formatted', iframes,root;
var checkForComposerAndApplyStyles = function(mutations) {
        if(root = document.getElementById('pm_composer')){
           iframes = root.querySelectorAll('iframe');
           for(i = 0; i < iframes.length; i++){
               if(!iframes[i].classList.contains(doneKey)){
                   iframes[i].classList.add(doneKey)
                   try{
                   applyStyles({target:iframes[i]});
                   }catch(err){console.log(err);}
               }
           }
        }
};
var applyStyles = function(e){
     var iWindow = e.target.contentWindow, iMutation, iObserver, i;
     setTimeout(() => {
        // style existing nodes before the signature
         for(i = 0; i < iWindow.document.body.children.length; i++){
             if(iWindow.document.body.children[i].className.match(/protonmail_signature_block/gi))break;
             if(iWindow.document.body.children[i].nodeName === 'DIV'){
                 iWindow.document.body.children[i].setAttribute('style',styles);
             }
         }
         // style nodes as added
         var addStylesToElement = function (iMutations){
             for(iMutation of iMutations){
                 iMutation.addedNodes.forEach(node => node.nodeName === 'DIV' && node.setAttribute('style',styles));
             }
         };
         iObserver = new iWindow.MutationObserver(addStylesToElement);
         try{
             iObserver.observe(iWindow.document.body, { childList: true });
         } catch(err){
             iObserver.disconnect();
             console.error(err);
         }
     },750);
 };
observer = new MutationObserver(checkForComposerAndApplyStyles);
try{
    observer.observe(document.body, { childList: true });

} catch(err){
    observer.disconnect();
    console.error(err);
}
})();